<?php declare(strict_types=1);

namespace ISINew\Decorator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Storefront\Controller\AuthController;
use Shopware\Storefront\Controller\StorefrontController;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\Checkout\Customer\SalesChannel\AbstractLogoutRoute;
use Shopware\Core\Framework\Validation\Exception\ConstraintViolationException;

//use Shopware\Storefront\Controller\AuthController as OriginAuthController;
/**
 * this file decorate the AthController 
 * Author : Hakim DJABRI <hakim.djabri@gmail.com
 * the needs is to redirecte the logout Action (account/logout) to the home page (/),
 *  instead of redirection to account login (account/login)
 */


/**
 * @RouteScope(scopes={"storefront"})
 */
class DecoratorAuthController extends StorefrontController //AuthController//
{
    
    /**
     * @var AuthController
     */
    private $decoratedController;

    /**
     * @var AbstractLogoutRoute
     */
    private $logoutRoute;

    /**
     * @var SystemConfigService
     */
    private $systemConfig;

    public function __construct(
        AuthController $decoratedController,
        SystemConfigService $systemConfig,
        AbstractLogoutRoute $logoutRoute        
    ) {
        $this->decoratedController = $decoratedController;
        $this->logoutRoute = $logoutRoute;
        $this->systemConfig = $systemConfig;
    }

    /**
     * @Route("/account/logout", name="frontend.account.logout.page", methods={"GET"})
     */
    public function logout(Request $request, SalesChannelContext $context): Response
    {
        if ($context->getCustomer() === null) {
            return $this->redirectToRoute('frontend.account.login.page');
        }
        
        //dd('Ich bin hier logout decoreted');
        try {            
            $this->logoutRoute->logout($context);            
            $salesChannelId = $context->getSalesChannel()->getId();            
            if ($request->hasSession() && $this->systemConfig->get('core.loginRegistration.invalidateSessionOnLogOut', $salesChannelId)) {
                $request->getSession()->invalidate();
            }            
            $this->addFlash('success', $this->trans('account.logoutSucceeded'));

            $parameters = [];
        } catch (ConstraintViolationException $formViolations) {
            $parameters = ['formViolations' => $formViolations];
        }
        // I have changed the redirect in login page to hame page
        return $this->redirectToRoute('frontend.home.page', $parameters);
    }   
    
    /**
     * @Route("/account/login", name="frontend.account.login.page", methods={"GET"})
     */
    public function loginPage(Request $request, RequestDataBag $data, SalesChannelContext $context): Response
    {
       return $this->decoratedController->loginPage($request,$data, $context);
    }
 /**
     * @Route("/account/login", name="frontend.account.login", methods={"POST"}, defaults={"XmlHttpRequest"=true})
     */
    public function login(Request $request, RequestDataBag $data, SalesChannelContext $context): Response
    {
        return $this->decoratedController->login($request,$data, $context);
    }

    /**
     * @Route("/account/recover", name="frontend.account.recover.page", methods={"GET"})
     *
     * @throws CategoryNotFoundException
     * @throws InconsistentCriteriaIdsException
     * @throws MissingRequestParameterException
     */
    public function recoverAccountForm(Request $request, SalesChannelContext $context): Response
    {
        return $this->decoratedController->recoverAccountForm($request, $context);
    }

    /**
     * @Route("/account/recover", name="frontend.account.recover.request", methods={"POST"})
     */
    public function generateAccountRecovery(Request $request, RequestDataBag $data, SalesChannelContext $context): Response
    {
        return $this->decoratedController->generateAccountRecovery($request,$data, $context);
    }

    /**
     * @Route("/account/recover/password", name="frontend.account.recover.password.page", methods={"GET"})
     *
     * @throws CategoryNotFoundException
     * @throws InconsistentCriteriaIdsException
     * @throws MissingRequestParameterException
     */
    public function resetPasswordForm(Request $request, SalesChannelContext $context): Response
    {
        return $this->decoratedController->resetPasswordForm($request, $context);
    }

    /**
     * @Route("/account/recover/password", name="frontend.account.recover.password.reset", methods={"POST"})
     *
     * @throws InconsistentCriteriaIdsException
     */
    public function resetPassword(RequestDataBag $data, SalesChannelContext $context): Response
    {
        return $this->decoratedController->resetPassword($data, $context);
    }   
    
}
