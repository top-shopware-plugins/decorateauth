===
=== Task 1 : creating a new theme : the main steps are : 
===
1.1 # run this commande inside the project directory to create a new theme
$ bin/console theme:create MyTheme

1.2 # run this to let shopware know about your plugin
$ bin/console plugin:refresh

1.3 # run this to install and activate your plugin
$ bin/console plugin:install --activate MyTheme

1.4 # run this to change the current storefront theme
$ bin/console theme:change

# you will get an interactive prompt to change the 
# current theme of the storefront like this
	
Please select a sales channel:
[0] Storefront | 64bbbe810d824c339a6c191779b2c205
[1] Headless | 98432def39fc4624b33213a56b8c944d
> 0

Please select a theme:
[0] Storefront
[1] MyTheme
> 1

Now The theme is fully installed and we can start our customization.

===
=== Task 2 : redirecte the logout Action to the home page : the main steps are : 
===
the first question is : 
how we can redirect the response of the logout action, without changing anything in the core of shopware
(the existing route is redirecting to "/account/login" page , but the new one will redirect to the / home page ).

1.1 # To do this redirection we need to override the AuthController (Decorating AuthController service)
the second question is :
How to decorate (overwrite, override ) the existing AuthController service via our plugin ISINew.

[hash]: <>(article:based_on_how_to_service_decoration)

## Overview

Decorating a service with our plugin is simple. The concept is when I need to overriding an existing definition, the original service is lost. 
In this case, the old service should be kept around to be able to reference it in the new one. This configuration replaces `Shopware\Storefront\Controller\AuthController` with a new one, but keeps a reference of the old one as `ISINew\Decorator\AuthController.inner`

## Decorating a service

The main requirement here is to have a `services.xml` file loaded in our plugin.
This can be achieved by placing the file into a `Resources/config` directory relative to our plugin's base class location.

Here's the content of `services.xml`:


```xml
<?xml version="1.0" ?>

<container xmlns="http://symfony.com/schema/dic/services"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://symfony.com/schema/dic/services http://symfony.com/schema/dic/services/services-1.0.xsd">

    <services>
       <!-- decorator !-->
         <service id="ISINew\Decorator\DecoratorAuthController" decorates="Shopware\Storefront\Controller\AuthController">
            <argument type="service" id="Shopware\Core\System\SystemConfig\SystemConfigService"/>
            <argument type="service" id="Shopware\Core\Checkout\Customer\SalesChannel\LogoutRoute"/>
            <call method="setContainer">
                <argument type="service" id="service_container"/>
            </call>
            <argument type="service" id="ISINew\Decorator\DecoratorAuthController.inner" />
        </service>
    </services>
</container>
```

The new AuthController (Decorator) ISINew\Decorator\DecoratorAuthController contains this change :

A new redirection route, I replaced the frontend.account.login.page (login page) by the frontend.home.page (home page)
```php
<?php declare(strict_types=1);

namespace ISINew\Decorator;
// using the same use of the Oroginal AuthController line 4 to 34
use Shopware\Core\Framework\Context;

/**
 * @RouteScope(scopes={"storefront"})
 */
class AuthController extends StorefrontController
{
    // using the same priority of the Oroginal AuthController line 40 to 74
    /**
     * @var AuthController
     */
    private $decoratedController;

    /**
     * @var AbstractLogoutRoute
     */
    private $logoutRoute;

    /**
     * @var SystemConfigService
     */
    private $systemConfig;

    public function __construct(
        AuthController $decoratedController,
        SystemConfigService $systemConfig,
        AbstractLogoutRoute $logoutRoute        
    ) {
        $this->decoratedController = $decoratedController;
        $this->logoutRoute = $logoutRoute;
        $this->systemConfig = $systemConfig;
    }

    /**
     * @Route("/account/logout", name="frontend.account.logout.page", methods={"GET"})
     */
    public function logout(Request $request, SalesChannelContext $context): Response
    {
        if ($context->getCustomer() === null) {
            return $this->redirectToRoute('frontend.account.login.page');
        }
        
        //dd('Ich bin hier logout decoreted');
        try {            
            $this->logoutRoute->logout($context);            
            $salesChannelId = $context->getSalesChannel()->getId();            
            if ($request->hasSession() && $this->systemConfig->get('core.loginRegistration.invalidateSessionOnLogOut', $salesChannelId)) {
                $request->getSession()->invalidate();
            }            
            $this->addFlash('success', $this->trans('account.logoutSucceeded'));

            $parameters = [];
        } catch (ConstraintViolationException $formViolations) {
            $parameters = ['formViolations' => $formViolations];
        }
        // I have changed the redirect in login page to hame page
        //Their is My new redirection route : 
        //I replaced the frontend.account.login.page (login page) by the frontend.home.page (home page)
        return $this->redirectToRoute('frontend.home.page', $parameters);
    }   
    
    /**
     * @Route("/account/login", name="frontend.account.login.page", methods={"GET"})
     */
    public function loginPage(Request $request, RequestDataBag $data, SalesChannelContext $context): Response
    {
       return $this->decoratedController->loginPage($request,$data, $context);
    }
      
// .....  line  141 to 303
 
}
```
===================
=== detail of creation of the new theme plugin
==============

[titleEn]: <>(Creating a new theme)
[hash]: <>(article:theme_create)

## Difference between "themes" and "regular" plugins

There are basically two ways to change the appearance of the storefront :
1- "regular" plugins which main purpose is to add new functions and change the behavior of the shop. 
2- "theme" plugins. The main purpose of themes is to change the appearance of the storefront and they behave a bit different compared to "regular" plugins.

Technically a theme is also a plugin but it will not only appear in the plugin manager of the administration,
it will also be visible in the theme manger once activated in the plugin manager.

To distinguish a theme plugin from a "regular" plugin you need to implement the Interface `Shopware\Storefront\Framework\ThemeInterface`
A theme can inherit from other themes, overwrite the default configuration (colors, fonts, media) and
add new configuration options.

## Creating my new theme

Open terminal and run this command to create a new theme.

```bash
# run this inside the project directory to create a new theme
$ bin/console theme:create MyTheme
	
# we should get an output like this:
	
Creating theme structure under .../development/custom/plugins/MyTheme
```

## Installing my new theme

```bash
# run this to let shopware know about your plugin
$ bin/console plugin:refresh

# you should get an output like this

[OK] Plugin list refreshed

Shopware Plugin Service
=======================

--------------------------- ------------------------- ------------- ----------------- -------------------- ----------- -------- ------------- 
Plugin                      Label                     Version       Upgrade version   Author               Installed   Active   Upgradeable  
--------------------------- ------------------------- ------------- ----------------- -------------------- ----------- -------- ------------- 
MyTheme                     Theme MyTheme plugin      6.2                                          No          No       No           
--------------------------- ------------------------- ------------- ----------------- -------------------- ----------- -------- -------------
```

Now we can install our new theme and activate it with these following command.

```bash
# run this to install and activate your plugin
$ bin/console plugin:install --activate MyTheme

# we should get an output like this

Shopware Plugin Lifecycle Service
=================================

Install 1 plugin(s):
* Theme MyTheme plugin (6.2)

Plugin "MyTheme" has been installed and activated successfully.


[OK] Installed 1 plugin(s).
```

## Changing current theme

```bash
# run this to change the current storefront theme
$ bin/console theme:change
	
# you will get an interactive prompt to change the 
# current theme of the storefront like this
	
Please select a sales channel:
[0] Storefront | 64bbbe810d824c339a6c191779b2c205
[1] Headless | 98432def39fc4624b33213a56b8c944d
> 0

Please select a theme:
[0] Storefront
[1] MyTheme
> 1

Set "MyTheme" as new theme for sales channel "Storefront"
Compiling theme 13e0a4a46af547479b1347617926995b for sales channel MyTheme	
```

Now your theme is fully installed and you can start your customization.

## Directory structure of a theme

In the file tree below you can see the basic file structure of the generated theme:

```
# move into your theme folder
$ cd custom/plugins/MyTheme
	
# structure of theme
├── composer.json
└── src
    ├── MyTheme.php
    └── Resources
        ├── app
        │   └── storefront
        │       ├── dist
        │       │   └── storefront
        │       │       └── js
        │       │           └── my-theme.js
        │       └── src
        │           ├── assets
        │           ├── main.js
        │           └── scss
        │               ├── base.scss
        │               └── overrides.scss
        └── theme.json
```

## Commands

The theme system can be controlled via CLI with the following commands.

### Theme refresh

Normally new themes are detected automatically but if you want to trigger this process
run the command

```bash
# run 
$ bin/console theme:refresh
```

### Change a theme
After scanning for themes these can be activated using 
```bash
# run this to interactively change a theme
$ bin/console theme:change

#run this to change a theme for all sales channels
$ bin/console theme:change MyCustomTheme --all
```

Notice: `theme:change` will also compile the theme for all new assignments. 

### Compile a theme

Calling the `theme:compile` command will recompile all themes which are assigned to a sales channel.

```bash
# run this to compile the scss and js files
$ bin/console theme:compile
```

## Browser Compatibility Note

Administration: All browsers in the latest version (Edge, (no IE), Chrome, Firefox, Safari, Opera)

Storefront: All browsers in the latest version and additionally IE 11


=========================================================================================
===

[titleEn]: <>(Decorating AuthController service)
[metaDescriptionEn]: <>(This docummentation will teach how to decorate (overwrite) the existing AuthController service via our plugin ISINew, we try with an example of how we can redirect the response of the logout action (the existing route is redirecting to "/account/login" page ), but the new one will redirect to the / home page.)
[hash]: <>(article:based_on_how_to_service_decoration)

## Overview

Decorating a service with our plugin is simple. The concept is when I need to overriding an existing definition, the original service is lost. In this case, the old service should be kept around to be able to reference it in the new one. This configuration replaces Shopware\Storefront\Controller\AuthController with a new one, but keeps a reference of the old one as ISINew\Decorator\AuthController.inner

## Decorating a service

The main requirement here is to have a `services.xml` file loaded in our plugin.
This can be achieved by placing the file into a `Resources/config` directory relative to our plugin's base class location.

Here's the content of `services.xml`:


